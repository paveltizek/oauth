<?php namespace Cerbero\Oauth;

use Illuminate\Support\ServiceProvider,
	\TwitterOAuth as Twitter,
	\YahooOAuthApplication as Yahoo;

class OauthServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('cerbero/oauth');

		include __DIR__ . '/filters.php';

		include __DIR__ . '/routes.php';
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->_registerStorage();

		$this->_registerProviders();
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array
		(
			'google' => array(),

			'facebook' => array('album', 'app'),

			'twitter' => array(),

			'yahoo' => array(),
		);
	}

	/**
	 * Register the token storage.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	protected function _registerStorage()
	{
		$this->app['oauth._storage'] = $this->app->share(function($app)
		{
			return new Storage\TokenStorage
			(
				new Storage\LaravelStorage
				(
					$app['session']->driver( $app['config']['oauth::_settings.storage'] )
				)
			);
		});
	}

	/**
	 * Register all providers.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	protected function _registerProviders()
	{
		$providers = $this->provides();

		foreach ($providers as $provider => $services)
		{
			$method = 'register' . ucfirst($provider);

			call_user_func(array($this, $method));

			$this->_registerServices($provider, $services);
		}
	}

	/**
	 * Register all services of a provider.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$provider
	 * @param	array	$services
	 * @return	void
	 */
	protected function _registerServices($provider, $services)
	{
		$Provider = ucfirst($provider);

		foreach ($services as $service)
		{
			$Service = ucfirst($service);

			$this->app->bind
			(
				"oauth.{$provider}.services.{$service}",

				"Cerbero\Oauth\Providers\Services\\{$Provider}\\{$Service}"
			);
		}
	}

	/**
	 * Register the Google provider.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	protected function registerGoogle()
	{
		$this->app['oauth.google'] = $this->app->share(function($app)
		{
			return new OAuth
			(
				new Providers\Google
				(
					new \Google_Client,

					$app->make('oauth._storage'),

					$app['config']['oauth::google']
				)
			);
		});
	}

	/**
	 * Register the Facebook provider.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	protected function registerFacebook()
	{
		$this->app['oauth.facebook'] = $this->app->share(function($app)
		{
			return new Oauth
			(
				new Providers\Facebook
				(
					new Providers\Clients\Facebook
					(
						$app->make('oauth._storage'),

						$app['config']['oauth::facebook']
					)
				)
			);
		});
	}

	/**
	 * Register the Twitter provider.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	protected function registerTwitter()
	{
		$this->app['oauth.twitter'] = $this->app->share(function($app)
		{
			$config = $app['config']['oauth::twitter'];

			return new Oauth
			(
				new Providers\Twitter
				(
					new Twitter($config['consumer_key'], $config['consumer_secret']),

					$app->make('oauth._storage')
				)
			);
		});
	}

	/**
	 * Register the Yahoo! provider.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	protected function registerYahoo()
	{
		$this->app['oauth.yahoo'] = $this->app->share(function($app)
		{
			$config = $app['config']['oauth::yahoo'];

			return new Oauth
			(
				new Providers\Yahoo
				(
					new Yahoo($config['consumer_key'], $config['consumer_secret'], $config['application_id']),

					$app->make('oauth._storage')
				)
			);
		});
	}

}