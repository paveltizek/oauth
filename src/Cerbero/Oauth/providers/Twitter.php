<?php namespace Cerbero\Oauth\Providers;

use Cerbero\Oauth\Storage\TokenStorageInterface;

/**
 * Twitter provider.
 *
 * @author	Andrea Marco Sartori
 */
class Twitter extends AbstractProvider
{

	/**
	 * Set the dependencies.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	TwitterOAuth	$client
	 * @param	Cerbero\Oauth\Storage\TokenStorageInterface	$storage
	 * @return	void
	 */
	public function __construct(\TwitterOAuth $client, TokenStorageInterface $storage)
	{
		$this->client = $client;

		$this->storage = $storage;
	}

	/**
	 * Retrieve the authorization URL.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	array|null	$scopes
	 * @return	string
	 */
	public function getAuthUrl($scopes = null)
	{
		$callback = array('oauth_callback' => $this->getRedirectUri());

		$token = $this->client->getRequestToken($callback);

		$this->storage->set('twitter.temp', $token);

		return $this->client->getAuthorizeURL($token);
	}

	/**
	 * Store the access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function storeAccessToken()
	{
		$inputs = \Input::only('oauth_token', 'oauth_verifier');

		$this->setAccessToken($inputs['oauth_token']);

		$token = $this->client->getAccessToken($inputs['oauth_verifier']);

		$this->storage->set('twitter', $token);
	}

	/**
	 * Set the access token.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$token
	 * @return	void
	 */
	protected function setAccessToken($token)
	{
		$tokens = $this->storage->get('twitter.temp');

		$this->client->setOAuthToken($token, $tokens['oauth_token_secret']);
	}

	/**
	 * Return the client.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	mixed
	 */
	public function getClient()
	{
		return $this->client;
	}

	/**
	 * Retrieve the unique user identifier.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	mixed
	 */
	public function getID()
	{
		$data = $this->storage->get('twitter');

		return $data['user_id'];
	}

	/**
	 * Retrieve the user email.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	UnexpectedValueException
	 */
	public function getEmail()
	{
		throw new \UnexpectedValueException("Twitter rules don't allow to retrieve the user eMail.");
	}

}