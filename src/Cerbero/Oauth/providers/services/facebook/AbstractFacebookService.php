<?php namespace Cerbero\Oauth\Providers\Services\Facebook;

use Cerbero\Oauth\Providers\Services\AbstractService;

/**
 * Facebook service abstraction.
 *
 * @author	Andrea Marco Sartori
 */
abstract class AbstractFacebookService extends AbstractService
{

	/**
	 * Retrieve a resource.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	mixed
	 */
	public function get()
	{
		$id = $this->getAttributes();

		return $this->api($id);
	}

	/**
	 * Shorthand to perform API calls.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$path
	 * @param	string	$method
	 * @param	array	$params
	 * @return	mixed
	 */
	protected function api($path, $method = 'GET', $params = array())
	{
		return $this->client()->api($path, $method, $params);
	}

	/**
	 * Retrieve the correct privacy.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$alias
	 * @return	array
	 */
	protected function getPrivacy($alias)
	{
		if(is_array($alias)) return $alias;

		$privacy = $this->replacePrivacy($alias);

		return array('value' => $privacy);
	}

	/**
	 * Replace alias with the correct privacy value.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$alias
	 * @return	string
	 */
	protected function replacePrivacy($alias)
	{
		$aliases = array('all', 'fof', 'friends', 'me');

		$values = array('EVERYONE', 'FRIENDS_OF_FRIENDS', 'ALL_FRIENDS', 'SELF');

		return str_replace($aliases, $values, $alias);
	}

}