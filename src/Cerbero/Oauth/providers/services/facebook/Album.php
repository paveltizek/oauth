<?php namespace Cerbero\Oauth\Providers\Services\Facebook;

/**
 * Service for albums.
 *
 * @author	Andrea Marco Sartori
 */
class Album extends AbstractFacebookService
{

	/**
	 * @author	Andrea Marco Sartori
	 * @var		array	$photos	Photos to add to an album.
	 */
	protected $photos = array();

	/**
	 * Retrieve all photos.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function photos()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/photos");
	}

	/**
	 * Add a photo.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	mixed	$source
	 * @param	string|null	$message
	 * @return	int
	 */
	public function addPhoto($source, $message = '')
	{
		$id = $this->getAttributes();

		$this->setPhotos($source, $message);

		return $this->sendPhotosTo($id);
	}

	/**
	 * Set photos to add.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	mixed	$source
	 * @param	string	$message
	 * @return	void
	 */
	protected function setPhotos($source, $message)
	{
		$this->photos[] = array($source, $message);
	}

	/**
	 * Send photos to an album.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	int	$id
	 * @return	int
	 */
	protected function sendPhotosTo($id)
	{
		foreach ($this->photos as $photo)
		{
			list($source, $message) = $photo;

			$source = curl_file_create($source->getRealPath(), $source->getMimeType());

			$id = head($this->api("{$id}/photos", 'POST', compact('source', 'message')));
		}
		return $id;
	}

	/**
	 * Retrieve all likes.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function likes()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/likes");
	}

	/**
	 * Like an album.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	boolean
	 */
	public function like()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/likes", 'POST');
	}

	/**
	 * Retrieve all comments.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function comments()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/comments");
	}

	/**
	 * Add a comment.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$message
	 * @return	int
	 */
	public function addComment($message)
	{
		$id = $this->getAttributes();

		return head($this->api("{$id}/comments", 'POST', compact('message')));
	}

	/**
	 * Create an album.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$name
	 * @param	string|null	$message
	 * @param	array	$privacy
	 * @return	int
	 */
	public function create($name, $message = '', $privacy = 'all')
	{
		$privacy = $this->getPrivacy($privacy);

		$id = head($this->api('me/albums', 'POST', compact('name', 'message', 'privacy')));

		if( ! empty($this->photos)) $this->sendPhotosTo($id);

		return $id;
	}

	/**
	 * Allow photo addition via method chaining.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	mixed	$source
	 * @param	string	$message
	 * @return	Cerbero\Oauth\Providers\Services\Facebook\Album	$this
	 */
	public function with($source, $message = '')
	{
		$this->setPhotos($source, $message);

		return $this;
	}

}