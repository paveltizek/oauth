<?php namespace Cerbero\Oauth\Providers\Services\Facebook;

/**
 * Service for applications.
 *
 * @author	Andrea Marco Sartori
 */
class App extends AbstractFacebookService
{

	/**
	 * Retrieve all test users.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function testUsers()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/accounts");
	}

	/**
	 * Create a test user.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	boolean	$installed
	 * @param	array	$permissions
	 * @param	string	$name
	 * @return	array
	 */
	public function addTestUser($installed = true, $permissions = array(), $name = '')
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/accounts/test-users", 'POST', compact('installed', 'permissions', 'name'));
	}

	/**
	 * Retrieve all albums.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function albums()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/albums");
	}

	/**
	 * Retrieve the wall.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function wall()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/feed");
	}

	/**
	 * Add a post to the wall.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$message
	 * @param	string	$link
	 * @param	mixed	$picture
	 * @param	string	$name
	 * @param	string	$caption
	 * @param	string	$description
	 * @param	array	$actions
	 * @param	string	$privacy
	 * @return	int
	 */
	public function addPost($message, $link, $picture = '', $name = '', $caption = '', $description = '', $actions = array(), $privacy = 'all')
	{
		$id = $this->getAttributes();

		$privacy = $this->getPrivacy($privacy);

		return head($this->api("{$id}/feed", 'POST', compact('message', 'link', 'picture', 'name', 'caption', 'description', 'actions', 'privacy')));
	}

	/**
	 * Add a status to the wall.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$message
	 * @return	int
	 */
	public function addStatus($message)
	{
		$id = $this->getAttributes();

		return head($this->api("{$id}/feed", 'POST', compact('message')));
	}

	/**
	 * Retrieve the usage metrics.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function insights()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/insights");
	}

	/**
	 * Retrieve all links.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function links()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/links");
	}

	/**
	 * Add a link to the wall.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$link
	 * @param	string	$message
	 * @return	int
	 */
	public function addLink($link, $message = '')
	{
		$id = $this->getAttributes();

		return head($this->api("{$id}/links", 'POST', compact('link', 'message')));
	}

	/**
	 * Retrieve all posts.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function posts()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/posts");
	}

	/**
	 * Retrieve all reviews.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function reviews()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/reviews");
	}

	/**
	 * Retrieve the resources usage.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function resources()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/staticresources");
	}

	/**
	 * Retrieve all statuses.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function statuses()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/statuses");
	}

	/**
	 * Retrieve all subscriptions.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function subscriptions()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/subscriptions");
	}

	/**
	 * Subscribe to an object.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$object
	 * @param	string	$callback_url
	 * @param	string	$fields
	 * @param	string	$verify_token
	 * @return	boolean
	 */
	public function subscribe($object, $callback_url, $fields = '', $verify_token = '')
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/subscriptions", 'POST', compact('object', 'callback_url', 'fields', 'verify_token'));
	}

	/**
	 * Remove a subscription.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$object
	 * @return	boolean
	 */
	public function unsubscribe($object = '')
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/subscriptions", 'DELETE', compact('object'));
	}

	/**
	 * Retrieve all sources where the application is tagged.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function tagged()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/tagged");
	}

	/**
	 * Retrieve all translations.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function translations()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/translations");
	}

	/**
	 * Add the translations.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	array	$native_strings
	 * @return	int
	 */
	public function translate(array $native_strings)
	{
		$id = $this->getAttributes();

		return head($this->api("{$id}/translations", 'POST', compact('native_strings')));
	}

	/**
	 * Remove the translations.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	array	$native_hashes
	 * @return	int
	 */
	public function removeTranslations(array $native_hashes)
	{
		$id = $this->getAttributes();

		return head($this->api("{$id}/translations", 'DELETE', compact('native_hashes')));
	}

	/**
	 * Retrieve all scores.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function scores()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/scores");
	}

	/**
	 * Remove the scores.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	boolean
	 */
	public function removeScores()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/scores", 'DELETE');
	}

	/**
	 * Retrieve all achievements.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	public function achievements()
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/achievements");
	}

	/**
	 * Create an achievement.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$achievement
	 * @param	int 	$display_order
	 * @return	boolean
	 */
	public function achieve($achievement, $display_order)
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/achievements", 'POST', compact('achievement', 'display_order'));
	}

	/**
	 * Remove an achievements.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$achievement
	 * @return	boolean
	 */
	public function removeAchievement($achievement)
	{
		$id = $this->getAttributes();

		return $this->api("{$id}/achievements", 'DELETE', compact('achievement'));
	}

}