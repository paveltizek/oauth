<?php namespace Cerbero\Oauth\Storage;

use \Illuminate\Session\Store;

/**
 * Use Laravel drivers as storage.
 *
 * @author	Andrea Marco Sartori
 */
class LaravelStorage implements StorageInterface
{

	/**
	 * Set the Laravel driver.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	Illuminate\Session\Store	$storage
	 * @return	void
	 */
	public function __construct(Store $storage)
	{
		$this->storage = $storage;
	}

	/**
	 * Store a value in the given key.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$key
	 * @return	mixed
	 */	
	public function put($key, $value)
	{
		$this->storage->put($key, $value);
	}

	/**
	 * Retrieve the stored value.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$key
	 * @return	mixed
	 */
	public function get($key)
	{
		return $this->storage->get($key);
	}

	/**
	 * Remove the stored value.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$key
	 * @return	void
	 */
	public function forget($key)
	{
		$this->storage->forget($key);
	}

}