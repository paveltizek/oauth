<?php namespace Cerbero\Oauth\Storage;

/**
 * Storage interface.
 *
 * @author	Andrea Marco Sartori
 */
interface StorageInterface
{

	/**
	 * Store a value in the given key.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$key
	 * @return	mixed
	 */	
	public function put($key, $value);

	/**
	 * Retrieve the stored value.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$key
	 * @return	mixed
	 */
	public function get($key);

	/**
	 * Remove the stored value.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$key
	 * @return	void
	 */
	public function forget($key);

}