<?php

return array(

	/**
	 * Client ID provided by Google
	 */
	'client_id' => '',

	/**
	 * Client secret key provided by Google
	 */
	'client_secret' => ''

);