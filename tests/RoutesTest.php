<?php

use \Mockery as m;

/**
 * Tests about package routes.
 *
 * @author	Andrea Marco Sartori
 */
class RoutesTest extends TestCase
{

	/**
	 * @testdox	Access token is set and the user is redirected.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testAccessTokenIsSetAndTheUserIsRedirected()
	{
		$google = m::mock('Cerbero\Oauth\Providers\Google')->shouldReceive('authorize')->once()->mock();

		App::shouldReceive('make')->once()->with('oauth.google')->andReturn($google);

		Redirect::shouldReceive('to')->once();

		$this->call('GET', 'login/oauth/google');
	}

}