<?php

/**
 * Tests about the IoC container.
 *
 * @author	Andrea Marco Sartori
 */
class ServiceProviderTest extends TestCase
{

	/**
	 * Initialise classes to test against.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function setUp()
	{
		parent::setUp();
		
		$this->service = new Cerbero\Oauth\OauthServiceProvider($this->app);
	}

	/**
	 * @testdox	Token storage is loaded.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testTokenStorageIsLoaded()
	{
		$storage = $this->app->make('oauth._storage');

		$this->assertInstanceOf('Cerbero\Oauth\Storage\TokenStorageInterface', $storage);
	}

	/**
	 * @testdox	Google provider is loaded.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testGoogleProviderIsLoaded()
	{
		$this->assertArrayHasKey('google', $this->service->provides());

		$oauth = $this->app->make('oauth.google');

		$this->assertInstanceOf('Cerbero\Oauth\OauthInterface', $oauth);

		$google = $oauth->client();

		$this->assertInstanceOf('Google_Client', $google);
	}

	/**
	 * @testdox	Facebook provider is loaded.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testFacebookProviderIsLoaded()
	{
		$this->assertArrayHasKey('facebook', $this->service->provides());

		$oauth = $this->app->make('oauth.facebook');

		$this->assertInstanceOf('Cerbero\Oauth\OauthInterface', $oauth);

		$facebook = $oauth->client();

		$this->assertInstanceOf('BaseFacebook', $facebook);
	}

	/**
	 * @testdox	Twitter provider is loaded.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testTwitterProviderIsLoaded()
	{
		$this->assertArrayHasKey('twitter', $this->service->provides());

		$oauth = $this->app->make('oauth.twitter');

		$this->assertInstanceOf('Cerbero\Oauth\OauthInterface', $oauth);

		$twitter = $oauth->client();

		$this->assertInstanceOf('TwitterOAuth', $twitter);
	}

	/**
	 * @testdox	Yahoo provider is loaded.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testYahooProviderIsLoaded()
	{
		$this->assertArrayHasKey('yahoo', $this->service->provides());

		$oauth = $this->app->make('oauth.yahoo');

		$this->assertInstanceOf('Cerbero\Oauth\OauthInterface', $oauth);

		$yahoo = $oauth->client();

		$this->assertInstanceOf('YahooOAuthApplication', $yahoo);
	}

}