<?php

/**
 * Base class for testing purpose.
 *
 * @author	Andrea Marco Sartori
 */
abstract class TestCase extends Orchestra\Testbench\TestCase
{

	/**
	 * Add package service provider.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
	protected function getPackageProviders()
	{
		return array('Cerbero\Oauth\OauthServiceProvider');
	}

	/**
	 * Add package aliases.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	array
	 */
    protected function getPackageAliases()
    {
        return array
        (
            'Google' => 'Cerbero\Oauth\Facades\Google',

            'Facebook' => 'Cerbero\Oauth\Facades\Facebook',

            'Twitter' => 'Cerbero\Oauth\Facades\Twitter',

            'Yahoo' => 'Cerbero\Oauth\Facades\Yahoo',
        );
    }

	/**
	 * Bind the service.
	 *
	 * @author	Andrea Marco Sartori
	 * @param	string	$serviceName
	 * @param	string	$provider
	 * @return	void
	 */
	protected function bindService($serviceName, $provider)
	{
		$class = 'Cerbero\Oauth\Providers\Services\\' . ucfirst($provider) . '\\' . ucfirst($serviceName);

		$service = new $class;

		$service->setAttributes(array(5));

		$mock = Mockery::mock(array($serviceName => $service, 'client' => $this->client));

		$this->app->instance("oauth.{$provider}", $mock);
	}

}