<?php

use Mockery as m;

/**
 * Tests about Facebook albums.
 *
 * @author	Andrea Marco Sartori
 */
class AlbumServiceTest extends TestCase
{

	/**
	 * Initialise classes to test against.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function setUp()
	{
		parent::setUp();

		$this->client = m::mock('Cerbero\Oauth\Providers\Clients\Facebook');
	}

	/**
	 * Clean up mocked objects.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function tearDown()
	{
		m::close();
	}

	/**
	 * Bind the album service.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	protected function bindAlbum()
	{
		$this->bindService('album', 'facebook');
	}

	/**
	 * @testdox	Callable by facade.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testCallableByFacade()
	{
		$album = Facebook::album();

		$service = 'Cerbero\Oauth\Providers\Services\Facebook\Album';

		$this->assertInstanceOf($service, $album);
	}

	/**
	 * @testdox	Retrieve an album.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAnAlbum()
	{
		$this->client->shouldReceive('api')->once()->with(5, 'GET', array());

		$this->bindAlbum();

		Facebook::album(5)->get();
	}

	/**
	 * @testdox	Retrieve all photos of an album.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAllPhotosOfAnAlbum()
	{
		$this->client->shouldReceive('api')->once()->with('5/photos', 'GET', array());

		$this->bindAlbum();

		Facebook::album(5)->photos();
	}

	/**
	 * @testdox	Add a photo.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testAddAPhoto()
	{
		$mock = m::mock(array('getRealPath' => '/path', 'getMimeType' => 'image/png'));

		$params = array('source' => curl_file_create('/path', 'image/png'), 'message' => 'desc');

		$this->client->shouldReceive('api')->once()->with('5/photos', 'POST', $params)->andReturn(array('id' => 3));

		$this->bindAlbum();

		$id = Facebook::album(5)->addPhoto($mock, 'desc');

		$this->assertSame(3, $id);
	}

	/**
	 * @testdox	Retrieve all likes.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAllLikes()
	{
		$this->client->shouldReceive('api')->once()->with('5/likes', 'GET', array());

		$this->bindAlbum();

		Facebook::album(5)->likes();
	}

	/**
	 * @testdox	Like an album.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testLikeAnAlbum()
	{
		$this->client->shouldReceive('api')->once()->with('5/likes', 'POST', array());

		$this->bindAlbum();

		Facebook::album(5)->like();
	}

	/**
	 * @testdox	Retrieve all comments.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAllComments()
	{
		$this->client->shouldReceive('api')->once()->with('5/comments', 'GET', array());

		$this->bindAlbum();

		Facebook::album(5)->comments();
	}

	/**
	 * @testdox	Add a comment.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testAddAComment()
	{
		$this->client->shouldReceive('api')->once()->with('5/comments', 'POST', array('message' => 'comment'))->andReturn(array('id' => 6));

		$this->bindAlbum();

		$id = Facebook::album(5)->addComment('comment');

		$this->assertSame(6, $id);
	}

	/**
	 * @testdox	Create an album.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testCreateAnAlbum()
	{
		$privacy = array('value' => 'EVERYONE');

		$params = array('name' => 'name', 'message' => 'description', 'privacy' => $privacy);

		$this->client->shouldReceive('api')->once()->with('me/albums', 'POST', $params)->andReturn(array('id' => 9));

		$this->bindAlbum();

		$id = Facebook::album()->create('name', 'description');

		$this->assertSame(9, $id);
	}

	/**
	 * @testdox	Add a photo during album creation.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testAddAPhotoDuringAlbumCreation()
	{
		$privacy = array('value' => 'EVERYONE');

		$params1 = array('name' => 'album', 'message' => 'albumDesc', 'privacy' => $privacy);

		$mock = m::mock(array('getRealPath' => '/path', 'getMimeType' => 'image/png'));

		$params2 = array('source' => curl_file_create('/path', 'image/png'), 'message' => 'photoDesc');

		$this->client->shouldReceive('api')->once()->with('me/albums', 'POST', $params1)->andReturn(array('id' => 12))->mock()
					 ->shouldReceive('api')->once()->with('12/photos', 'POST', $params2)->andReturn(array('id' => 15));

		$this->bindAlbum();

		$id = Facebook::album()->with($mock, 'photoDesc')->create('album', 'albumDesc');

		$this->assertSame(12, $id);
	}

	/**
	 * @testdox	Set privacy to EVERYONE when its alias is used.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testSetPrivacyToEVERYONEWhenItsAliasIsUsed()
	{
		$params = array('name' => 'album', 'message' => 'desc', 'privacy' => array('value' => 'EVERYONE'));

		$this->client->shouldReceive('api')->once()->with('me/albums', 'POST', $params)->andReturn(array('id' => 18));

		$this->bindAlbum();

		Facebook::album()->create('album', 'desc', 'all');
	}

	/**
	 * @testdox	Set privacy to FRIENDS_OF_FRIENDS when its alias is used.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testSetPrivacyToFRIENDS_OF_FRIENDSWhenItsAliasIsUsed()
	{
		$params = array('name' => 'album', 'message' => 'desc', 'privacy' => array('value' => 'FRIENDS_OF_FRIENDS'));

		$this->client->shouldReceive('api')->once()->with('me/albums', 'POST', $params)->andReturn(array('id' => 18));

		$this->bindAlbum();

		Facebook::album()->create('album', 'desc', 'fof');
	}

	/**
	 * @testdox	Set privacy to ALL_FRIENDS when its alias is used.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testSetPrivacyToALL_FRIENDSWhenItsAliasIsUsed()
	{
		$params = array('name' => 'album', 'message' => 'desc', 'privacy' => array('value' => 'ALL_FRIENDS'));

		$this->client->shouldReceive('api')->once()->with('me/albums', 'POST', $params)->andReturn(array('id' => 18));

		$this->bindAlbum();

		Facebook::album()->create('album', 'desc', 'friends');
	}

	/**
	 * @testdox	Set privacy to SELF when its alias is used.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testSetPrivacyToSELFWhenItsAliasIsUsed()
	{
		$params = array('name' => 'album', 'message' => 'desc', 'privacy' => array('value' => 'SELF'));

		$this->client->shouldReceive('api')->once()->with('me/albums', 'POST', $params)->andReturn(array('id' => 18));

		$this->bindAlbum();

		Facebook::album()->create('album', 'desc', 'me');
	}

}