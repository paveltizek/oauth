<?php

use Mockery as m;

/**
 * Tests about Facebook applications.
 *
 * @author	Andrea Marco Sartori
 */
class AppServiceTest extends TestCase
{

	/**
	 * Initialise classes to test against.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function setUp()
	{
		parent::setUp();
		
		$this->client = m::mock('Cerbero\Oauth\Providers\Clients\Facebook');
	}

	/**
	 * Clean up mocked objects.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function tearDown()
	{
		m::close();
	}

	/**
	 * Bind the application service.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	protected function bindApp()
	{
		$this->bindService('app', 'facebook');
	}

	/**
	 * @testdox	Callable by facade.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testCallableByFacade()
	{
		$app = Facebook::app();

		$service = 'Cerbero\Oauth\Providers\Services\Facebook\App';

		$this->assertInstanceOf($service, $app);
	}

	/**
	 * @testdox	Retrieve an application.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAnApplication()
	{
		$this->client->shouldReceive('api')->once()->with(5, 'GET', array())->andReturn(array());

		$this->bindApp();

		$app = Facebook::app(5)->get();

		$this->assertInternalType('array', $app);
	}

	/**
	 * @testdox	Retrieve all test users.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAllTestUsers()
	{
		$this->client->shouldReceive('api')->once()->with('5/accounts', 'GET', array())->andReturn(array());

		$this->bindApp();

		$app = Facebook::app(5)->testUsers();

		$this->assertInternalType('array', $app);
	}

	/**
	 * @testdox	Create a test user.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testCreateATestUser()
	{
		$params = array('installed' => true, 'permissions' => array(), 'name' => '');

		$this->client->shouldReceive('api')->once()->with('5/accounts/test-users', 'POST', $params)->andReturn(array());

		$this->bindApp();

		$app = Facebook::app(5)->addTestUser();

		$this->assertInternalType('array', $app);
	}

	/**
	 * @testdox	Retrieve all albums.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAllAlbums()
	{
		$this->client->shouldReceive('api')->once()->with('5/albums', 'GET', array())->andReturn(array());

		$this->bindApp();

		$app = Facebook::app(5)->albums();

		$this->assertInternalType('array', $app);
	}

	/**
	 * @testdox	Retrieve the wall.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveTheWall()
	{
		$this->client->shouldReceive('api')->once()->with('5/feed', 'GET', array())->andReturn(array());

		$this->bindApp();

		$app = Facebook::app(5)->wall();

		$this->assertInternalType('array', $app);
	}

	/**
	 * @testdox	Add a post to the wall.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testAddAPostToTheWall()
	{
		$privacy = array('value' => 'EVERYONE');

		$params = array('message' => 'text', 'link' => 'linkUrl', 'picture' => '', 'name' => '', 'caption' => '', 'description' => '', 'actions' => array(), 'privacy' => $privacy);

		$this->client->shouldReceive('api')->once()->with('5/feed', 'POST', $params)->andReturn(array('id' => 2));

		$this->bindApp();

		$app = Facebook::app(5)->addPost('text', 'linkUrl');

		$this->assertSame(2, $app);
	}

	/**
	 * @testdox	Add a status to the wall.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testAddAStatusToTheWall()
	{
		$params = array('message' => 'hi');

		$this->client->shouldReceive('api')->once()->with('5/feed', 'POST', $params)->andReturn(array('id' => 3));

		$this->bindApp();

		$app = Facebook::app(5)->addStatus('hi');

		$this->assertSame(3, $app);
	}

	/**
	 * @testdox	Retrieve the usage metrics.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveTheUsageMetrics()
	{
		$this->client->shouldReceive('api')->once()->with('5/insights', 'GET', array())->andReturn(array());

		$this->bindApp();

		$app = Facebook::app(5)->insights();

		$this->assertInternalType('array', $app);
	}

	/**
	 * @testdox	Retrieve all links.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAllLinks()
	{
		$this->client->shouldReceive('api')->once()->with('5/links', 'GET', array())->andReturn(array());

		$this->bindApp();

		$app = Facebook::app(5)->links();

		$this->assertInternalType('array', $app);
	}

	/**
	 * @testdox	Add a link to the wall.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testAddALinkToTheWall()
	{
		$params = array('link' => 'linkUrl', 'message' => '');

		$this->client->shouldReceive('api')->once()->with('5/links', 'POST', $params)->andReturn(array('id' => 1));

		$this->bindApp();

		$app = Facebook::app(5)->addLink('linkUrl');

		$this->assertSame(1, $app);
	}

	/**
	 * @testdox	Retrieve all posts.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAllPosts()
	{
		$this->client->shouldReceive('api')->once()->with('5/posts', 'GET', array())->andReturn(array());

		$this->bindApp();

		$app = Facebook::app(5)->posts();

		$this->assertInternalType('array', $app);
	}

	/**
	 * @testdox	Retrieve all reviews.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAllReviews()
	{
		$this->client->shouldReceive('api')->once()->with('5/reviews', 'GET', array())->andReturn(array());

		$this->bindApp();

		$app = Facebook::app(5)->reviews();

		$this->assertInternalType('array', $app);
	}

	/**
	 * @testdox	Retrieve the resources usage.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveTheResourcesUsage()
	{
		$this->client->shouldReceive('api')->once()->with('5/staticresources', 'GET', array())->andReturn(array());

		$this->bindApp();

		$app = Facebook::app(5)->resources();

		$this->assertInternalType('array', $app);
	}

	/**
	 * @testdox	Retrieve all statuses.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAllStatuses()
	{
		$this->client->shouldReceive('api')->once()->with('5/statuses', 'GET', array())->andReturn(array());

		$this->bindApp();

		$app = Facebook::app(5)->statuses();

		$this->assertInternalType('array', $app);
	}

	/**
	 * @testdox	Retrieve all subscriptions.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAllSubscriptions()
	{
		$this->client->shouldReceive('api')->once()->with('5/subscriptions', 'GET', array())->andReturn(array());

		$this->bindApp();

		$app = Facebook::app(5)->subscriptions();

		$this->assertInternalType('array', $app);
	}

	/**
	 * @testdox	Subscribe to an object.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testSubscribeToAnObject()
	{
		$params = array('object' => 'page', 'callback_url' => 'exampleUrl', 'fields' => '', 'verify_token' => '');

		$this->client->shouldReceive('api')->once()->with('5/subscriptions', 'POST', $params)->andReturn(true);

		$this->bindApp();

		$app = Facebook::app(5)->subscribe('page', 'exampleUrl');

		$this->assertTrue($app);
	}

	/**
	 * @testdox	Remove a subscription.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRemoveASubscription()
	{
		$this->client->shouldReceive('api')->once()->with('5/subscriptions', 'DELETE', array('object' => null))->andReturn(true);

		$this->bindApp();

		$app = Facebook::app(5)->unsubscribe();

		$this->assertTrue($app);
	}

	/**
	 * @testdox	Retrieve all sources where the application is tagged.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAllSourcesWhereTheApplicationIsTagged()
	{
		$this->client->shouldReceive('api')->once()->with('5/tagged', 'GET', array())->andReturn(array());

		$this->bindApp();

		$app = Facebook::app(5)->tagged();

		$this->assertInternalType('array', $app);
	}

	/**
	 * @testdox	Retrieve all translations.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAllTranslations()
	{
		$this->client->shouldReceive('api')->once()->with('5/translations', 'GET', array())->andReturn(array());

		$this->bindApp();

		$app = Facebook::app(5)->translations();

		$this->assertInternalType('array', $app);
	}

	/**
	 * @testdox	Add the translations.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testAddTheTranslations()
	{
		$params = array('native_strings' => array('foo'));

		$this->client->shouldReceive('api')->once()->with('5/translations', 'POST', $params)->andReturn(array('id' => 4));

		$this->bindApp();

		$app = Facebook::app(5)->translate(array('foo'));

		$this->assertSame(4, $app);
	}

	/**
	 * @testdox	Remove the translations.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRemoveTheTranslations()
	{
		$params = array('native_hashes' => array('foo'));

		$this->client->shouldReceive('api')->once()->with('5/translations', 'DELETE', $params)->andReturn(array('id' => 6));

		$this->bindApp();

		$app = Facebook::app(5)->removeTranslations(array('foo'));

		$this->assertSame(6, $app);
	}

	/**
	 * @testdox	Retrieve all scores.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAllScores()
	{
		$this->client->shouldReceive('api')->once()->with('5/scores', 'GET', array())->andReturn(array());

		$this->bindApp();

		$app = Facebook::app(5)->scores();

		$this->assertInternalType('array', $app);
	}

	/**
	 * @testdox	Remove the scores.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRemoveTheScores()
	{
		$this->client->shouldReceive('api')->once()->with('5/scores', 'DELETE', array())->andReturn(true);

		$this->bindApp();

		$app = Facebook::app(5)->removeScores();

		$this->assertTrue($app);
	}

	/**
	 * @testdox	Retrieve all achievements.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRetrieveAllAchievements()
	{
		$this->client->shouldReceive('api')->once()->with('5/achievements', 'GET', array())->andReturn(array());

		$this->bindApp();

		$app = Facebook::app(5)->achievements();

		$this->assertInternalType('array', $app);
	}

	/**
	 * @testdox	Create an achievement.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testCreateAnAchievement()
	{
		$params = array('achievement' => 'achUrl', 'display_order' => 100);

		$this->client->shouldReceive('api')->once()->with('5/achievements', 'POST', $params)->andReturn(true);

		$this->bindApp();

		$app = Facebook::app(5)->achieve('achUrl', 100);

		$this->assertTrue($app);
	}

	/**
	 * @testdox	Remove an achievement.
	 *
	 * @author	Andrea Marco Sartori
	 * @return	void
	 */
	public function testRemoveAnAchievement()
	{
		$params = array('achievement' => 'achUrl');

		$this->client->shouldReceive('api')->once()->with('5/achievements', 'DELETE', $params)->andReturn(true);

		$this->bindApp();

		$app = Facebook::app(5)->removeAchievement('achUrl');

		$this->assertTrue($app);
	}

}